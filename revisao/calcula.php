<?php
    //var_dump($_POST);
    //exit();
    $destino = $_POST["destino"];
    $quantidade = $_POST["quantidade"];
    $valor = $destino*$quantidade;
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Calculo</title>

</head>
<body>
    <h1>Resultado do Cálculo</h1>
    <p>R$ <?= number_format($valor, 2, ','); ?></p>
    <a href="./passagens.php">Recalcular</a>
</body>
</html>