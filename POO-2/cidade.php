<?php

    require_once "./conexao.php";

    class Cidade extends Conexao{
        private $id, $nome, $UF;

        function __construct($nome, $UF){
            parent::__construct();
            $this->id = null;
            $this->nome = $nome;
            $this->UF = $UF;
        }

        public function setNome(string $nome){
            $this->nome = $nome;
        }

        public function setUF(string $UF){
            $this->UF = $UF;
        }

        public function salvar(){
            $sql = "INSERT INTO cidades(nome, UF) values(?, ?)";
            $query = $this->conexao->prepare($sql);
            $resultado = $query->execute([$this->nome, $this->UF]);
            if($resultado)
                $this->id = $this->conexao->lastInsertId();
            else
                die("Erro ao tentar inserir os dados");
        }

        public function atualizar(){
            if($this->id == null){
                die("Cidade não encontrada!");
            }
            $sql = "UPDATE cidades SET nome=?, UF=? WHERE id=?";
            $query = $this->conexao->prepare($sql);
            $resultado = $query->execute([$this->nome, $this->UF, $this->id]);
            if(!$resultado)
                die("Erro ao tentar atualizar os dados");
        }

        public function deletar($id=null){
            if(!$id){
                $id = $this->id;
            }
            $sql = "DELETE FROM cidades WHERE id = ?";
            $query = $this->conexao->prepare($sql);
            $resultado = $query->execute([$id]);
            if(!$resultado)
                die("Erro ao tentar deletar a cidade");
        }

        static function buscar($id){
            $conexao = Conexao::conexao();
            $sql = "SELECT * FROM cidades WHERE id = ?";
            $query = $conexao->prepare($sql);
            $resultado = $query->execute([$id]);
            return $query->fetch();
        }
   }