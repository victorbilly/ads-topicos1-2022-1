<?php

    class AlunoModel extends Conexao{
        private $tabela;

        function __construct(){
            parent::__construct(); //construtor da classe pai
            $this->tabela = "alunos";
        }

        public function get($id=Null){
            if($id){
                $consulta = "SELECT * FROM $this->tabela WHERE id = ?";
                $query = $this->conexao->prepare($consulta);
                $query->execute([$id]);
                $objeto = $query->fetch();
                return $objeto;
            }
            $consulta = $this->conexao->query("SELECT * FROM $this->tabela");
            $lista = $consulta->fetchAll();
            return $lista;
        }

        public function remover($id){ 
            $query = $this->conexao->prepare("DELETE FROM $this->tabela WHERE id=?");
            return $query->execute(array($id));
        }

        public function salvar($dados){
            if(isset($dados['id']))
                $sql = "UPDATE $this->tabela set nome = :nome, idade = :idade WHERE id = :id";               
            else
                $sql = "INSERT INTO $this->tabela(nome, idade) values(:nome, :idade)";
            $query = $this->conexao->prepare($sql);
            $resultado = $query->execute($dados);
            if($resultado)
                echo "Dados salvos com sucesso!";
        }
    }

?>