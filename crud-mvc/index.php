<?php

    require_once "./config/conexao.php";
    
    function init(){
        $controlador = $_GET['controlador'];
        include_once ('./controller/'.$controlador.'_controller.php');
        
        $classe = ucfirst($controlador)."Controller";
        $controller = new $classe();

        $acao = isset($_GET['acao'])? $_GET['acao'] : 'listar';
        if($acao == "deletar" || $acao == "buscar"){
            $id = $_GET['id'];
            $controller->{$acao}($id);
        }else if ($acao=="inserir"){
            $controller->{$acao}($_POST);
        }else if ($acao=="atualizar"){
            $id = $_GET['id'];
            $controller->{$acao}($id, $_POST);
        }else{
            $controller->{$acao}();
        }
    }

    init();
    

    
