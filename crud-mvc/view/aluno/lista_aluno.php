<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

    <title>Lista de Alunos</title>
  </head>
  <body>
    <div class="container">
        <div class="d-grid gap-2 d-md-flex justify-content-md-end">
            <a href="index.php?controlador=aluno&acao=novo" class="btn btn-info">Adicionar</a>
        </div>
        <table class="table table-stripped table-hover">
            <thead>
                <th>Id</th>
                <th>Nome</th>
                <th>Idade</th>
                <th>Ações</th>
            </thead>
            <tbody>
                <?php foreach($lista as $linha): ?>
                    <tr>
                        <td> <?= $linha["id"]; ?> </td>
                        <td> <?= $linha["nome"]; ?> </td>
                        <td> <?= $linha["idade"]; ?> </td>
                        <td> 
                            <a href="./index.php?controlador=aluno&acao=buscar&id=<?= $linha['id']; ?>" class="btn btn-warning">
                                Atualizar
                            </a> 
                            <a href="./index.php?controlador=aluno&acao=deletar&id=<?= $linha['id']; ?>" class="btn btn-danger">
                                Deletar
                            </a> 
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
  </body>
</html>
