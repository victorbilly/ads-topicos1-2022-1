<?php

    require_once "./model/aluno_model.php";

    class AlunoController extends AlunoModel{
        
        function __construct(){
            parent::__construct();            
        }

        public function redirect(){
            header("Location:index.php?controlador=aluno");
        }

        public function listar(){
            $lista = $this->get();
            require_once "./view/aluno/lista_aluno.php";
        }

        public function novo(){
            $acao = "index.php?controlador=aluno&acao=inserir";
            require_once "./view/aluno/form_aluno.php";
        }

        public function buscar($id){
            $aluno = $this->get($id);
            $acao = "index.php?controlador=aluno&acao=atualizar&id=$id";
            require_once "./view/aluno/form_aluno.php";
        }

        public function inserir($dados){
            $this->salvar($dados);
            $this->redirect();
        }

        public function atualizar($id, $dados){
            $dados['id'] = $id;
            $this->salvar($dados);
            $this->redirect();
        }

        public function deletar($id){
            $resultado = $this->remover($id);
            if ($resultado)
                $this->redirect();
        }
    }

    
    

?>