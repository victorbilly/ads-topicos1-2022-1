<?php

    require "./pessoa.php";
    
    $cliente1 = new Pessoa("Pedro", "Rua dos pe junto");

    $nome = "Pedro";
    $endereco = "Rua dos pé junto";
    $cliente2 = new Pessoa($nome, $endereco);

    echo $cliente1->getNome();
    $cliente1->setNome("Pedro Ortaça"); 
    echo $cliente1->getNome();

    $cliente3 = new Pessoa("Carlos");
    $cliente4 = new Pessoa();

    $cliente5 = new Pessoa('', "Rua do Ipes");

    $cliente1->setAltura(1.91);
    $cliente1->setPeso(120);
    $imc = $cliente1->calculaIMC();

    $nome = $cliente1->getNome();
    echo "<br/>Cliente $nome, imc: $imc";