<?php

    require_once "pessoa.php";

    class Cliente extends Pessoa{

        private $telefone;
        private $telefone_residencial;

        function __construct($nome, $telefone, $telefone_residencial=null){
            parent::__construct($nome);
            $this->telefone = $telefone;
            $this->telefone_residencial = $telefone_residencial;
        }

        public function setTelefone($telefone){
            $this->telefone = $telefone;
        }

        public function getTelefone(){
            return $this->telefone;
        }

        public function setTelefoneResidencial($telefone_residencial) {
            $this->telefone_residencial = $telefone_residencial;
        }

    }