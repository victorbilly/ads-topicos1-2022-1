<?php

class Pessoa{

    private $nome;
    private $endereco;
    private $altura;
    private $peso;

    function __construct($nome=null, $endereco=null){
        $this->nome = $nome;
        $this->endereco = $endereco;
    }

    public function getEndereco(){
        return $this->endereco;
    }

    public function setEndereco($endereco){
        $this->endereco = $endereco;
    }

    public function getNome(){
        return $this->nome;
    }

    public function setNome($nome){
        $this->nome = $nome;
    }

    public function setAltura($altura){
        $this->altura = $altura;
    }

    public function setPeso($peso){
        $this->peso = $peso;
    }

    public function calculaIMC(){
        $imc = $this->peso/($this->altura*$this->altura);
        return $imc;
    }

    static function calcImc($peso, $altura){
        return $peso/($altura*$altura);
    }
    
} 


